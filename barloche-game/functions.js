let enemiesCreated = [];
let timeResisted = [];
let dead = false;
let menu = false;

//Permettant de faire spawn un ennemi et de lui attribuer une vitesse
//Créé aussi l'intervalle de temps entre les différents spawn et choisis le nombre spawnant en même temps

function enemySpawn(enemy, count) {
        if (!menu) {
                let currentPos = 1300;
                let enemyCreated = document.createElement("img");
                enemyCreated.id = enemy.id;
                enemyCreated.src = enemy.src;
                playground.appendChild(enemyCreated);
                enemyCreated.style.top = [Math.floor(Math.random() * Math.floor(500) + 60)] + "px";
                enemyCreated.style.left = currentPos + "px";
                enemy.diff += 2;
                enemiesCreated.push(enemyCreated);
                let interval = setInterval(function () {
                        if (!menu) {
                                currentPos -= enemy.diff;
                                enemyCreated.style.left = currentPos + "px";

                                if (enemyCreated.offsetLeft - 70 < barloche.offsetLeft - 70 + barloche.offsetWidth - 70 &&
                                        enemyCreated.offsetLeft - 70 + enemyCreated.offsetWidth - 70 > barloche.offsetLeft - 70 &&
                                        enemyCreated.offsetTop - 20 < barloche.offsetTop - 20 + barloche.offsetHeight - 20 &&
                                        enemyCreated.offsetHeight - 50 + enemyCreated.offsetTop - 50 > barloche.offsetTop - 20) {
                                        barloche.remove();

                                        if (timeResisted.length < 5) {
                                                dead = true;
                                                for (const enemies of enemiesCreated) {
                                                        enemies.remove();
                                                }
                                                alert("Game over, you resisted " + (timeResisted.length) + " seconds.");
                                        }

                                        if (timeResisted.length > 5) {
                                                dead = true;
                                                alert("Well played, you resisted " + (timeResisted.length) + " seconds.");
                                                for (const enemies of enemiesCreated) {
                                                        enemies.remove();
                                                }
                                        }
                                };

                                if (currentPos <= -400) {
                                        enemyCreated.remove();
                                        clearInterval(interval);
                                };
                        };
                }, 50);
        };

        if (!dead) {
                setTimeout(function () {
                        enemySpawn(enemies[Math.floor(Math.random() * Math.floor(4))], count);
                }, Math.floor(Math.random() * Math.floor(1000) + 500));
        };
};

//Permet de créer une attaque à partir de la position de son personnage et faire qu'elle se déplace 
//de la gauche vers la droite.

function attack(attack) {
        let currentPosTop = barloche.offsetTop;
        let currentPosLeft = barloche.offsetLeft + 200;
        let attackCreated = document.createElement("img");
        attackCreated.id = attack.id;
        attackCreated.src = attack.src;
        playground.appendChild(attackCreated);
        attackCreated.style.top = currentPosTop + "px";
        attackCreated.style.left = currentPosLeft + "px";
        let interval = setInterval(function () {
                currentPosLeft += attack.speed;
                attackCreated.style.left = currentPosLeft + "px";
                for (const enemies of enemiesCreated) {

                        if (attackCreated.offsetLeft < enemies.offsetLeft + enemies.offsetWidth &&
                                attackCreated.offsetLeft + attackCreated.offsetWidth > enemies.offsetLeft &&
                                attackCreated.offsetTop - 20 < enemies.offsetTop - 20 + enemies.offsetHeight - 20 &&
                                attackCreated.offsetHeight - 50 + attackCreated.offsetTop - 50 > enemies.offsetTop - 20) {
                                enemies.remove();
                                attackCreated.remove();
                                clearInterval(interval);
                        };

                        if (currentPosLeft >= window.innerWidth) {
                                attackCreated.remove();
                                clearInterval(interval);
                        };
                };
        }, 50);
};

//Mon Timer

function countTime() {
        let timer = document.createElement("p");
        let counter = 1;
        timer.id = "timer";
        infoBar.appendChild(timer);
        let repeat = setInterval(function () {
                if (!menu) {
                        timer.textContent = counter + " s";
                        counter++;
                        timeResisted.push(counter);
                        if (counter > 100) {
                                timer.style.left = 13.5 + "%";
                        }
                        if (counter > 1000) {
                                timer.style.left = 13 + "%";
                        }
                        if (dead === true) {
                                clearInterval(repeat);
                        }
                }
        }, 1000);
        counter;
};


