
// Variables 

let body = document.body;
let infoBar = document.querySelector("#infoBar");
let playground = document.querySelector("#playground");
let barloche = document.querySelector("#barlave");
let menuCreated = document.querySelector("#menu");
let backToGame = document.querySelector("#menu1");
let options = document.querySelector("#menu2");
let backToMainMenu = document.querySelector("#menu3");
let menu2P = document.querySelector("#menu2P");
let menu3P = document.querySelector("#menu3P");
let difficulty = 5;

// Bouger son personnage sur la hauteur 

playground.addEventListener("mousemove", function (event) {
        if (!menu) {
                barloche.style.top = event.y + "px";
                if (barloche.offsetTop >= 605) {
                        barloche.style.top = 604 + "px";
                }
        };
});

// Lancer une attaque

body.addEventListener("keypress", function (event) {
        if (event.code === "Space") {
                attack(attacks[0]);
        };
});

//Faire apparaître et disparaître le menu

body.addEventListener("keydown", function (event) {
        
        if (event.code === "Escape") {
                if (!menu) {
                        menu = true;
                        body.style.cursor = "pointer";
                        menuCreated.style.display = "initial"
                } else {
                        menu = false;
                        body.style.cursor = "none";
                        menuCreated.style.display = "none";
                }
        }
});

backToGame.addEventListener("click", function () {
        menu = false;
        body.style.cursor = "none";
        menuCreated.style.display = "none";
});

options.addEventListener("click", function () {
        menu2P.innerHTML = "Coming soon";
        setTimeout(function () {
                menu2P.innerHTML = "Options";
        }, 1000);
});

backToMainMenu.addEventListener("click", function () {
        menu3P.innerHTML = "Coming soon";
        setTimeout(function () {
                menu3P.innerHTML = "Back to main menu";
        }, 1000);
});

//Appel de fonction

enemySpawn(enemies[3]);
let countedTime = countTime();



